<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ColdDrinksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cold Drinks';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="cold-drinks-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php
        if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 1) {
            ?>
            <p>
                <?= Html::a('Create Cold Drinks', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php
        }
        ?>

        <?php
        if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 1) {

            ?>
            <?= GridView::widget([
                'id' => 'cold-drinks-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'ingredients',
                    //'recipe',

                    ['class' => 'yii\grid\ActionColumn']
                ],
            ]);
            ?>
            <?php
        } else if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 0) {
            ?>
            <?= GridView::widget([
                'id' => 'cold-drinks-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'ingredients',
                    //'recipe',
                    ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                    ['class' => 'yii\grid\CheckboxColumn', 'multiple' => false]
                ],
            ]);
            ?>
            <?php
        } else {
            ?>
            <?= GridView::widget([
                'id' => 'cold-drinks-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'ingredients',
                    //'recipe'
                ],
            ]);
            ?>
            <?php
        } ?>

        <?php
        if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 0) {
            ?>
            <p>
                <?= Html::SubmitButton('purchase', ['class' => 'btn btn-success', 'id' => 'purchase']) ?>
            </p>
            <?php
        }
        ?>

        <?php Pjax::end(); ?>
    </div>

<?php
$script = <<< JS
$(function () {
         $('#purchase').click(function(){
            var pk = $('#cold-drinks-grid').yiiGridView('getSelectedRows');
            window.alert("selected: " + pk);
            
            $.post( {
                url: ColdDrinksController / actionPurchase,
                dataType: 'json',
                data: {keylist: pk},
                success: function(data) {
                    alert('Purchase is done...')
                }
            });
            
         });
});
JS;
$this->registerJs($script);
?>