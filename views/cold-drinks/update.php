<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ColdDrinks */

$this->title = 'Update Cold Drinks: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cold Drinks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cold-drinks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
