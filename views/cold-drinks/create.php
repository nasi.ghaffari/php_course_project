<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ColdDrinks */

$this->title = 'Create Cold Drinks';
$this->params['breadcrumbs'][] = ['label' => 'Cold Drinks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cold-drinks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
