<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Desserts */

$this->title = 'Create Desserts';
$this->params['breadcrumbs'][] = ['label' => 'Desserts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desserts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
