<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DessertsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Desserts';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="desserts-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php
        if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 1) {
            ?>
            <p>
                <?= Html::a('Create Desserts', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php
        }
        ?>

        <?php
        if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 1) {

            ?>
            <?= GridView::widget([
                'id' => 'desserts-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'ingredients',
                    //'recipe',

                    ['class' => 'yii\grid\ActionColumn']
                ],
            ]);
            ?>
            <?php
        } else if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 0) {
            ?>
            <?= GridView::widget([
                'id' => 'desserts-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'ingredients',
                    //'recipe',
                    ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                    ['class' => 'yii\grid\CheckboxColumn', 'multiple' => false]
                ],
            ]);
            ?>
            <?php
        } else {
            ?>
            <?= GridView::widget([
                'id' => 'desserts-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'ingredients',
                    //'recipe'
                ],
            ]);
            ?>
            <?php
        } ?>

        <?php
        if (Yii::$app->user->identity && Yii::$app->user->identity->is_admin == 0) {
            ?>
            <p>
                <?= Html::a('Purchase', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php
        }
        ?>

        <?php Pjax::end(); ?>
    </div>

<?php
$script = <<< JS
$(function () {
         $('#purchase').click(function(){
            var pk = $('#desserts-grid').yiiGridView('getSelectedRows');
            window.alert("selected: " + pk);
            
            $.post( {
                url: DessertsController / actionPurchase,
                dataType: 'json',
                data: {keylist: pk},
                success: function(data) {
                    alert('Purchase is done...')
                }
            });
            
         });
});
JS;
$this->registerJs($script);
?>