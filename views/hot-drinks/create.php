<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HotDrinks */

$this->title = 'Create Hot Drinks';
$this->params['breadcrumbs'][] = ['label' => 'Hot Drinks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hot-drinks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
