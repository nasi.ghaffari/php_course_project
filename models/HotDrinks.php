<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hot_drinks".
 *
 * @property int $id
 * @property string $name
 * @property string $ingredients
 * @property string $recipe
 */
class HotDrinks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hot_drinks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'ingredients', 'recipe'], 'required'],
            [['name'], 'string', 'max' => 30],
            [['ingredients'], 'string', 'max' => 50],
            [['recipe'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ingredients' => 'Ingredients',
            'recipe' => 'Recipe',
        ];
    }
}
