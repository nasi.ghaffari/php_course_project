<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchases".
 *
 * @property int $id
 * @property int $user_id
 * @property int $hot_drink_id
 * @property int $cold_drink_id
 * @property int $dessert_id
 * @property string $create_date
 */
class Purchases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'hot_drink_id', 'cold_drink_id', 'dessert_id'], 'integer'],
            [['create_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'hot_drink_id' => 'Hot Drink ID',
            'cold_drink_id' => 'Cold Drink ID',
            'dessert_id' => 'Dessert ID',
            'create_date' => 'Create Date',
        ];
    }
}
