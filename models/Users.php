<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\helpers\Security;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $password
 * @property int $is_admin
 * @property string $authKey
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'username', 'password', 'is_admin', 'authKey'], 'required'],
            [['is_admin'], 'integer'],
            [['first_name', 'last_name', 'username'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 100],
            [['authKey'], 'string', 'max' => 128],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'is_admin' => 'Is Admin',
            'authKey' => 'Auth Key',
        ];
    }

    /**
     * This method is used when you need to maintain the login status via session.
     * @param int|string $id
     * @return Users|null|IdentityInterface
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * it returns a key used to verify cookie-based login.
     * The key is stored in the login cookie and will be later compared with the server-side version
     * to make sure the login cookie is valid.
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param $password
     */
    /*public function setPassword($password)
    {
        try {
            $this->password = \Yii::$app->security->generatePasswordHash($password);
        } catch (Exception $e) {
        }
    }*/

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        try {
            $this->authKey = \Yii::$app->security->generateRandomString();
        } catch (Exception $e) {
        }
    }

    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        try {
            return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
        } catch (Exception $e) {
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param $password
     */
    public function setPassword($password)
    {
        try {
            $this->password = \Yii::$app->security->generatePasswordHash($password);
        } catch (Exception $e) {
        }
    }

    /**
     * @return int
     */
    public function isAdmin()
    {
        return $this->is_admin;
    }


}
